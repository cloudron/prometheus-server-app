#!/bin/bash

set -eu

mkdir -p /app/data/storage /app/data/config

if [[ ! -f /app/data/config/prometheus.yml ]]; then
    echo "=> Creating config file on first run"
    cp /app/code/prometheus.yml /app/data/config/prometheus.yml
fi

chown -R cloudron:cloudron /app/data

if [[ ! -f /app/data/env.sh ]]; then
    echo "=> Copy default env.sh"
    cp /app/pkg/env.sh.template /app/data/env.sh
fi

echo "=> Source custom variables for cli args"
source /app/data/env.sh

echo "=> Starting Prometheus"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/prometheus --config.file=/app/data/config/prometheus.yml ${cli_options}
