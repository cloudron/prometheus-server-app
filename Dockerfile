FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=prometheus/prometheus versioning=semver extractVersion=^v(?<version>.+)$
ARG PROMETHEUS_VERSION=3.2.1

RUN curl -L https://github.com/prometheus/prometheus/releases/download/v${PROMETHEUS_VERSION}/prometheus-${PROMETHEUS_VERSION}.linux-amd64.tar.gz | tar zxvf - --strip-components 1

COPY env.sh.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
