[0.1.0]
* Initial version

[0.2.0]
* Fix app title

[0.3.0]
* Fix first run command

[1.0.0]
* Initial stable version

[1.1.0]
* Use base image v3

[1.2.0]
* Update prometheus to 2.23.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.23.0)
* UI: Make the React UI default. #8142
* Remote write: The following metrics were removed/renamed in remote write. #6815
* Remote: Do not collect non-initialized timestamp metrics. #8060
* Remote write: Added a metric prometheus_remote_storage_max_samples_per_send for remote write. #8102
* TSDB: Make the snapshot directory name always the same length. #8138
* TSDB: Create a checkpoint only once at the end of all head compactions. #8067
* TSDB: Avoid Series API from hitting the chunks. #8050
* TSDB: Cache label name and last value when adding series during compactions making compactions faster. #8192

[1.3.0]
* Update Prometheus to 2.24.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.24.0)

[1.4.0]
* Update Prometheus to 2.25.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.25.0)
* Add optional name property to testgroup for better test failure output. #8440
* Add warnings into React Panel on the Graph page. #8427
* TSDB: Increase the number of buckets for the compaction duration metric. #8342
* Remote: Allow passing along custom remote_write HTTP headers. #8416
* Mixins: Scope grafana configuration. #8332

[1.4.1]
* Update Prometheus to 2.25.2
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.25.2)
* Fix the ingestion of scrapes when the wall clock changes, e.g. on suspend
* Enable basicAuth so it can work with apps like grafana

[1.5.0]
* Update Prometheus to 2.26.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.26.0)
* Remote: Add support for AWS SigV4 auth method for remote_write. #8509
* Scaleway Discovery: Add Scaleway Service Discovery. #8555
* PromQL: Allow negative offsets. Behind --enable-feature=promql-negative-offset flag. #8487
* experimental Exemplars: Add in-memory storage for exemplars. Behind --enable-feature=exemplar-storage flag. #6635
* UI: Add advanced auto-completion, syntax highlighting and linting to graph page query input. #8634

[1.6.0]
* Update Prometheus to 2.27.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.27.0)
* UI: Add a dark theme. #8604

[1.6.1]
* Update Prometheus to 2.27.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.27.1)

[1.7.0]
* Update Prometheus to 2.28.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.28.0)
* [CHANGE] UI: Make the new experimental PromQL editor the default. #8925
* [FEATURE] Linode SD: Add Linode service discovery. #8846
* [FEATURE] HTTP SD: Add generic HTTP-based service discovery. #8839
* [FEATURE] Kubernetes SD: Allow configuring API Server access via a kubeconfig file. #8811
* [FEATURE] UI: Add exemplar display support to the graphing interface. #8832 #8945 #8929
* [FEATURE] Consul SD: Add namespace support for Consul Enterprise. #8900
* [ENHANCEMENT] Promtool: Allow silencing output when importing / backfilling data. #8917
* [ENHANCEMENT] Consul SD: Support reading tokens from file. #8926
* [ENHANCEMENT] Rules: Add a new .ExternalURL alert field templating variable, containing the external URL of the Prometheus server. #8878
* [ENHANCEMENT] Scrape: Add experimental body_size_limit scrape configuration setting to limit the allowed response body size for target scrapes. #8833 #8886

[1.7.1]
* Update Prometheus to 2.28.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.28.1)
* [BUGFIX]: HTTP SD: Allow charset specification in Content-Type header. #8981
* [BUGFIX]: HTTP SD: Fix handling of disappeared target groups. #9019
* [BUGFIX]: Fix incorrect log-level handling after moving to go-kit/log. #9021

[1.8.0]
* Update Prometheus to 2.29.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.29.0)

[1.8.1]
* Update Prometheus to 2.29.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.29.1)

[1.8.2]
* Update Prometheus to 2.29.2
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.29.2)

[1.9.0]
* Update Prometheus to 2.30.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.30.0)

[1.9.1]
* Update Prometheus to 2.30.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.30.1)

[1.9.2]
* Update Prometheus to 2.30.2
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.30.2)

[1.9.3]
* Update Prometheus to 2.30.3
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.30.3)

[1.9.4]
* Set default retention_time to 15 days

[1.9.5]
* Update Prometheus to 2.31.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.31.0)
* [CHANGE] UI: Remove standard PromQL editor in favour of the codemirror-based editor. #9452
* [FEATURE] PromQL: Add trigonometric functions and atan2 binary operator. #9239 #9248 #9515
* [FEATURE] Remote: Add support for exemplar in the remote write receiver endpoint. #9319 #9414
* [FEATURE] SD: Add PuppetDB service discovery. #8883
* [FEATURE] SD: Add Uyuni service discovery. #8190
* [FEATURE] Web: Add support for security-related HTTP headers. #9546
* [ENHANCEMENT] Azure SD: Add proxy_url, follow_redirects, tls_config. #9267
* [ENHANCEMENT] Backfill: Add --max-block-duration in promtool create-blocks-from rules. #9511
* [ENHANCEMENT] Config: Print human-readable sizes with unit instead of raw numbers. #9361
* [ENHANCEMENT] HTTP: Re-enable HTTP/2. #9398
* [ENHANCEMENT] Kubernetes SD: Warn user if number of endpoints exceeds limit. #9467
* [ENHANCEMENT] OAuth2: Add TLS configuration to token requests. #9550
* [ENHANCEMENT] PromQL: Several optimizations. #9365 #9360 #9362 #9552
* [ENHANCEMENT] PromQL: Make aggregations deterministic in instant queries. #9459
* [ENHANCEMENT] Rules: Add the ability to limit number of alerts or series. #9260 #9541
* [ENHANCEMENT] SD: Experimental discovery manager to avoid restarts upon reload. Disabled by default, enable with flag --enable-feature=new-service-discovery-manager. #9349 #9537
* [ENHANCEMENT] UI: Debounce timerange setting changes. #9359
* [BUGFIX] Backfill: Apply rule labels after query labels. #9421
* [BUGFIX] Scrape: Resolve conflicts between multiple exported label prefixes. #9479 #9518
* [BUGFIX] Scrape: Restart scrape loops when __scrape_interval__ is changed. #9551
* [BUGFIX] TSDB: Fix memory leak in samples deletion. #9151
* [BUGFIX] UI: Use consistent margin-bottom for all alert kinds. #9318

[1.9.6]
* Update Prometheus to 2.31.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.31.1)
* Fix a panic when the experimental discovery manager receive targets during a reload

[1.9.7]
* Update Prometheus to 2.31.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.31.1)

[1.10.0]
* Update Prometheus to 2.32.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.32.0)

[1.10.1]
* Update Prometheus to 2.32.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.32.1)

[1.11.0]
* Update Prometheus to 2.33.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.33.0)

[1.11.1]
* Update Prometheus to 2.33.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.33.1)

[1.11.2]
* Update Prometheus to 2.33.2
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.33.2)

[1.11.3]
* Update Prometheus to 2.33.3
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.33.3)

[1.11.4]
* Update Prometheus to 2.33.4
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.33.4)

[1.12.0]
* Update Prometheus to 2.33.5
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.33.5)

[1.13.0]
* Update Prometheus to 2.34.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.34.0)
* TSDB: Disable the chunk write queue by default and allow configuration with the experimental flag --storage.tsdb.head-chunks-write-queue-size. #10425
* HTTP SD: Add a failure counter. #10372
* Azure SD: Set Prometheus User-Agent on requests. #10209
* Uyuni SD: Reduce the number of logins to Uyuni. #10072
* Scrape: Log when an invalid media type is encountered during a scrape. #10186
* Scrape: Accept application/openmetrics-text;version=1.0.0 in addition to version=0.0.1. #9431
* Remote-read: Add an option to not use external labels as selectors for remote read. #10254
* UI: Optimize the alerts page and add a search bar. #10142
* UI: Improve graph colors that were hard to see. #10179
* Config: Allow escaping of $ with $$ when using environment variables with external labels. #10129

[1.13.1]
* Update Prometheus to 2.35.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.35.0)

[1.14.0]
* Update Prometheus to 2.36.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.36.0)
* [FEATURE] Add lowercase and uppercase relabel action. #10641
* [FEATURE] SD: Add IONOS Cloud integration. #10514
* [FEATURE] SD: Add Vultr integration. #10714
* [FEATURE] SD: Add Linode SD failure count metric. #10673
* [FEATURE] Add prometheus_ready metric. #10682
* [ENHANCEMENT] Add stripDomain to template function. #10475
* [ENHANCEMENT] UI: Enable active search through dropped targets. #10668
* [ENHANCEMENT] promtool: support matchers when querying label values. #10727
* [ENHANCEMENT] Add agent mode identifier. #9638
* [BUGFIX] Changing TotalQueryableSamples from int to int64. #10549
* [BUGFIX] tsdb/agent: Ignore duplicate exemplars. #10595
* [BUGFIX] TSDB: Fix chunk overflow appending samples at a variable rate. #10607
* [BUGFIX] Stop rule manager before TSDB is stopped. #10680

[1.14.1]
* Update Prometheus to 2.36.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.36.1)
* [BUGFIX] promtool: Add --lint-fatal option #10840

[1.14.2]
* Update Prometheus to 2.36.2
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.36.2)
* [BUGFIX] Fix serving of static assets like fonts and favicon. #10888

[1.15.0]
* Update Prometheus to 2.37.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.37.0)
* Nomad SD: New service discovery for Nomad built-in service discovery. #10915
* Kubernetes SD: Allow attaching node labels for endpoint role. #10759
* PromQL: Optimise creation of signature with/without labels. #10667
* TSDB: Memory optimizations. #10873 #10874
* TSDB: Reduce sleep time when reading WAL. #10859 #10878
* OAuth2: Add appropriate timeouts and User-Agent header. #11020

[1.16.0]
* Update Prometheus to 2.38.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.38.0)
* Web: Add a /api/v1/format_query HTTP API endpoint that allows pretty-formatting PromQL expressions. #11036 #10544 #11005
* UI: Add support for formatting PromQL expressions in the UI. #11039
* DNS SD: Support MX records for discovering targets. #10099
* Templates: Add toTime() template function that allows converting sample timestamps to Go time.Time values. #10993
* PromQL: When a query panics, also log the query itself alongside the panic message. #10995

[1.17.0]
* Update Prometheus to 2.39.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.39.0)
* experimental TSDB: Add support for ingesting out-of-order samples. This is configured via out_of_order_time_window field in the config file; check config file docs for more info. #11075
* API: /-/healthy and /-/ready API calls now also respond to a HEAD request on top of existing GET support. #11160
* PuppetDB SD: Add `__meta_puppetdb_query` label. #11238
* AWS EC2 SD: Add `__meta_ec2_region label`. #11326
* AWS Lightsail SD: Add `__meta_lightsail_region` label. #11326
* Scrape: Optimise relabeling by re-using memory. #11147
* TSDB: Improve WAL replay timings. #10973 #11307 #11319
* TSDB: Optimise memory by not storing unnecessary data in the memory. #11280 #11288 #11296
* TSDB: Allow overlapping blocks by default. --storage.tsdb.allow-overlapping-blocks now has no effect. #11331
* UI: Click to copy label-value pair from query result to clipboard. #11229

[1.17.1]
* Update Prometheus to 2.39.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.39.1)
* [BUGFIX] Rules: Fix notifier relabel changing the labels on active alerts. #11427

[1.18.0]
* Update Prometheus to 2.40.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.40.0)

[1.18.1]
* Update Prometheus to 2.40.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.40.1)

[1.18.2]
* Update Prometheus to 2.40.2
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.40.2)

[1.18.3]
* Update Prometheus to 2.40.3
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.40.3)
* TSDB: Fix compaction after a deletion is called. #11623

[1.18.4]
* Update Prometheus to 2.40.4
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.40.4)
* Fix Basic Authentication Bypass (CVE-2022-46146)

[1.18.5]
* Update Prometheus to 2.40.5
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.40.5)
* [BUGFIX] TSDB: Fix queries involving native histograms due to improper reset of iterators. #11643

[1.18.6]
* Update Prometheus to 2.40.6
* Update Coudron base image to 4.0.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.40.6)

[1.18.7]
* Update Prometheus to 2.40.7
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.40.7)
* [BUGFIX] TSDB: Fix queries involving negative buckets of native histograms. #11699

[1.19.0]
* Update Prometheus to 2.41.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.41.0)

[1.20.0]
* Update Prometheus to 2.42.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.42.0)

[1.21.0]
* Update Prometheus to 2.43.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.43.0)

[1.21.1]
* Update Prometheus to 2.43.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.43.1)

[1.22.0]
* Update Prometheus to 2.44.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.44.0)
* [CHANGE] Remote-write: Raise default samples per send to 2,000. #12203
* [FEATURE] Remote-read: Handle native histograms. #12085, #12192
* [FEATURE] Promtool: Health and readiness check of prometheus server in CLI. #12096
* [FEATURE] PromQL: Add query_samples_total metric, the total number of samples loaded by all queries. #12251
* [ENHANCEMENT] Storage: Optimise buffer used to iterate through samples. #12326
* [ENHANCEMENT] Scrape: Reduce memory allocations on target labels. #12084
* [ENHANCEMENT] PromQL: Use faster heap method for topk() / bottomk(). #12190
* [ENHANCEMENT] Rules API: Allow filtering by rule name. #12270
* [ENHANCEMENT] Native Histograms: Various fixes and improvements. #11687, #12264, #12272
* [ENHANCEMENT] UI: Search of scraping pools is now case-insensitive. #12207

[1.23.0]
* Update Prometheus to 2.45.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.45.0)
* [FEATURE] API: New limit parameter to limit the number of items returned by /api/v1/status/tsdb endpoint. #12336
* [FEATURE] Config: Add limits to global config. #12126
* [FEATURE] Consul SD: Added support for path_prefix. #12372
* [FEATURE] Native histograms: Add option to scrape both classic and native histograms. #12350
* [FEATURE] Native histograms: Added support for two more arithmetic operators avg_over_time and sum_over_time. #12262
* [FEATURE] Promtool: When providing the block id, only one block will be loaded and analyzed. #12031
* [FEATURE] Remote-write: New Azure ad configuration to support remote writing directly to Azure Monitor workspace. #11944
* [FEATURE] TSDB: Samples per chunk are now configurable with flag storage.tsdb.samples-per-chunk. By default set to its former value 120. #12055
* [ENHANCEMENT] Native histograms: bucket size can now be limited to avoid scrape fails. #12254
* [ENHANCEMENT] TSDB: Dropped series are now deleted from the WAL sooner. #12297
* [BUGFIX] Native histograms: ChunkSeries iterator now checks if a new sample can be appended to the open chunk. #12185
* [BUGFIX] Native histograms: Fix Histogram Appender Appendable() segfault. #12357
* [BUGFIX] Native histograms: Fix setting reset header to gauge histograms in seriesToChunkEncoder. #12329
* [BUGFIX] TSDB: Tombstone intervals are not modified after Get() call. #12245
* [BUGFIX] TSDB: Use path/filepath to set the WAL directory. #12349

[1.24.0]
* Move storage directory to `/app/data/storage`
* CLI options can now be set in `/app/data/env.sh`

[1.25.0]
* Update Prometheus to 2.46.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.46.0)
* [FEATURE] Promtool: Add PromQL format and label matcher set/delete commands to promtool. #11411
* [FEATURE] Promtool: Add push metrics command. #12299
* [ENHANCEMENT] Promtool: Read from stdin if no filenames are provided in check rules. #12225
* [ENHANCEMENT] Hetzner SD: Support larger ID's that will be used by Hetzner in September. #12569
* [ENHANCEMENT] Kubernetes SD: Add more labels for endpointslice and endpoints role. #10914
* [ENHANCEMENT] Kubernetes SD: Do not add pods to target group if the PodIP status is not set. #11642
* [ENHANCEMENT] OpenStack SD: Include instance image ID in labels. #12502
* [ENHANCEMENT] Remote Write receiver: Validate the metric names and labels. #11688
* [ENHANCEMENT] Web: Initialize prometheus_http_requests_total metrics with code label set to 200. #12472
* [ENHANCEMENT] TSDB: Add Zstandard compression option for wlog. #11666
* [ENHANCEMENT] TSDB: Support native histograms in snapshot on shutdown. #12258
* [ENHANCEMENT] Labels: Avoid compiling regexes that are literal. #12434
* [BUGFIX] Histograms: Fix parsing of float histograms without zero bucket. #12577
* [BUGFIX] Histograms: Fix scraping native and classic histograms missing some histograms. #12554
* [BUGFIX] Histograms: Enable ingestion of multiple exemplars per sample. 12557
* [BUGFIX] File SD: Fix path handling in File-SD watcher to allow directory monitoring on Windows. #12488
* [BUGFIX] Linode SD: Cast InstanceSpec values to int64 to avoid overflows on 386 architecture. #12568
* [BUGFIX] PromQL Engine: Include query parsing in active-query tracking. #12418
* [BUGFIX] TSDB: Handle TOC parsing failures. #10623

[1.26.0]
* Update Prometheus to 2.47.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.47.0)
* [FEATURE] Web: Add OpenTelemetry (OTLP) Ingestion endpoint. #12571 #12643
* [FEATURE] Scraping: Optionally limit detail on dropped targets, to save memory. #12647
* [ENHANCEMENT] TSDB: Write head chunks to disk in the background to reduce blocking. #11818
* [ENHANCEMENT] PromQL: Speed up aggregate and function queries. #12682
* [ENHANCEMENT] PromQL: More efficient evaluation of query with timestamp(). #12579
* [ENHANCEMENT] API: Faster streaming of Labels to JSON. #12598
* [ENHANCEMENT] Agent: Memory pooling optimisation. #12651
* [ENHANCEMENT] TSDB: Prevent storage space leaks due to terminated snapshots on shutdown. #12664
* [ENHANCEMENT] Histograms: Refactoring and optimisations. #12352 #12584 #12596 #12711 #12054
* [ENHANCEMENT] Histograms: Add histogram_stdvar and histogram_stddev functions. #12614
* [ENHANCEMENT] Remote-write: add http.resend_count tracing attribute. #12676

[1.26.1]
* Update Prometheus to 2.47.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.47.1)
* [BUGFIX] Fix duplicate sample detection at chunk size limit #12874

[1.26.2]
* Update Prometheus to 2.47.2
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.47.2)

[1.27.0]
* Update Prometheus to 2.48.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.48.0)
* [CHANGE] Remote-write: respect Retry-After header on 5xx errors. #12677
* [FEATURE] Alerting: Add AWS SigV4 authentication support for Alertmanager endpoints. #12774
* [FEATURE] Promtool: Add support for histograms in the TSDB dump command. #12775
* [FEATURE] PromQL: Add warnings (and annotations) to PromQL query results. #12152 #12982 #12988 #13012
* [FEATURE] Remote-write: Add Azure AD OAuth authentication support for remote write requests. #12572
* [ENHANCEMENT] Remote-write: Add a header to count retried remote write requests. #12729
* [ENHANCEMENT] TSDB: Improve query performance by re-using iterator when moving between series. #12757
* [ENHANCEMENT] UI: Move /targets page discovered labels to expandable section #12824
* [ENHANCEMENT] TSDB: Optimize WBL loading by not sending empty buffers over channel. #12808
* [ENHANCEMENT] TSDB: Reply WBL mmap markers concurrently. #12801
* [ENHANCEMENT] Promtool: Add support for specifying series matchers in the TSDB analyze command. #12842
* [ENHANCEMENT] PromQL: Prevent Prometheus from overallocating memory on subquery with large amount of steps. #12734
* [ENHANCEMENT] PromQL: Add warning when monotonicity is forced in the input to histogram_quantile. #12931
* [ENHANCEMENT] Scraping: Optimize sample appending by reducing garbage. #12939
* [ENHANCEMENT] Storage: Reduce memory allocations in queries that merge series sets. #12938
* [ENHANCEMENT] UI: Show group interval in rules display. #12943
* [ENHANCEMENT] Scraping: Save memory when scraping by delaying creation of buffer. #12953
* [ENHANCEMENT] Agent: Allow ingestion of out-of-order samples. #12897
* [ENHANCEMENT] Promtool: Improve support for native histograms in TSDB analyze command. #12869
* [ENHANCEMENT] Scraping: Add configuration option for tracking staleness of scraped timestamps. #13060
* [BUGFIX] SD: Ensure that discovery managers are properly canceled. #10569
* [BUGFIX] TSDB: Fix PostingsForMatchers race with creating new series. #12558
* [BUGFIX] TSDB: Fix handling of explicit counter reset header in histograms. #12772
* [BUGFIX] SD: Validate HTTP client configuration in HTTP, EC2, Azure, Uyuni, PuppetDB, and Lightsail SDs. #12762 #12811 #12812 #12815 #12814 #12816
* [BUGFIX] TSDB: Fix counter reset edgecases causing native histogram panics. #12838
* [BUGFIX] TSDB: Fix duplicate sample detection at chunk size limit. #12874
* [BUGFIX] Promtool: Fix errors not being reported in check rules command. #12715
* [BUGFIX] TSDB: Avoid panics reported in logs when head initialization takes a long time. #12876
* [BUGFIX] TSDB: Ensure that WBL is repaired when possible. #12406
* [BUGFIX] Storage: Fix crash caused by incorrect mixed samples handling. #13055
* [BUGFIX] TSDB: Fix compactor failures by adding min time to histogram chunks. #13062

[1.27.1]
* Update Prometheus to 2.48.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.48.1)
* [BUGFIX] TSDB: Make the wlog watcher read segments synchronously when not tailing. #13224
* [BUGFIX] Agent: Participate in notify calls (fixes slow down in remote write handling introduced in 2.45). #13223

[1.28.0]
* Update Prometheus to 2.49.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.49.0)
* [FEATURE] Promtool: Add --run flag promtool test rules command. #12206
* [FEATURE] SD: Add support for NS records to DNS SD. #13219
* [FEATURE] UI: Add heatmap visualization setting in the Graph tab, useful histograms. #13096 #13371
* [FEATURE] Scraping: Add `scrape_config.enable_compression` (default true) to disable gzip compression when scraping the target. #13166
* [FEATURE] PromQL: Add a promql-experimental-functions feature flag containing some new experimental PromQL functions. #13103 NOTE: More experimental functions might be added behind the same feature flag in the future. Added functions:
* [FEATURE] SD: Add `__meta_linode_gpus` label to Linode SD. #13097
* [FEATURE] API: Add `exclude_alerts` query parameter to /api/v1/rules to only return recording rules. #12999
* [FEATURE] TSDB: --storage.tsdb.retention.time flag value is now exposed as a `prometheus_tsdb_retention_limit_seconds` metric. #12986
* [FEATURE] Scraping: Add ability to specify priority of scrape protocols to accept during scrape (e.g. to scrape Prometheus proto format for certain jobs). This can be changed by setting `global.scrape_protocols` and `scrape_config.scrape_protocols`. #12738

[1.28.1]
* Update Prometheus to 2.49.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.49.1)
* [BUGFIX] TSDB: Fixed a wrong q= value in scrape accept header #13313

[1.29.0]
* Update Prometheus to 2.50.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.50.0)
* [CHANGE] Remote Write: Error storage.ErrTooOldSample is now generating HTTP error 400 instead of HTTP error 500. #13335
* [FEATURE] Remote Write: Drop old inmemory samples. Activated using the config entry sample_age_limit. #13002
* [FEATURE] Experimental: Add support for ingesting zeros as created timestamps. (enabled under the feature-flag created-timestamp-zero-ingestion). #12733 #13279
* [FEATURE] Promtool: Add analyze histograms command. #12331
* [FEATURE] TSDB/compaction: Add a way to enable overlapping compaction. #13282 #13393 #13398
* [FEATURE] Add automatic memory limit handling. Activated using the feature flag. auto-gomemlimit #13395
* [ENHANCEMENT] Promtool: allow specifying multiple matchers in promtool tsdb dump. #13296
* [ENHANCEMENT] PromQL: Restore more efficient version of NewPossibleNonCounterInfo annotation. #13022
* [ENHANCEMENT] Kuma SD: Extend configuration to allow users to specify client ID. #13278
* [ENHANCEMENT] PromQL: Use natural sort in sort_by_label and sort_by_label_desc. This is experimental. #13411
* [ENHANCEMENT] Native Histograms: support native_histogram_min_bucket_factor in scrape_config. #13222
* [ENHANCEMENT] Native Histograms: Issue warning if histogramRate is applied to the wrong kind of histogram. #13392
* [ENHANCEMENT] TSDB: Make transaction isolation data structures smaller. #13015
* [ENHANCEMENT] TSDB/postings: Optimize merge using Loser Tree. #12878
* [ENHANCEMENT] TSDB: Simplify internal series delete function. #13261
* [ENHANCEMENT] Agent: Performance improvement by making the global hash lookup table smaller. #13262
* [ENHANCEMENT] PromQL: faster execution of metric functions, e.g. abs(), rate() #13446
* [ENHANCEMENT] TSDB: Optimize label values with matchers by taking shortcuts. #13426
* [ENHANCEMENT] Kubernetes SD: Check preconditions earlier and avoid unnecessary checks or iterations in kube_sd. #13408
* [ENHANCEMENT] Promtool: Improve visibility for promtool test rules with JSON colored formatting. #13342
* [ENHANCEMENT] Consoles: Exclude iowait and steal from CPU Utilisation. #9593
* [ENHANCEMENT] Various improvements and optimizations on Native Histograms. #13267, #13215, #13276 #13289, #13340
* [BUGFIX] Scraping: Fix quality value in HTTP Accept header. #13313
* [BUGFIX] UI: Fix usage of the function time() that was crashing. #13371
* [BUGFIX] Azure SD: Fix SD crashing when it finds a VM scale set. #13578

[1.29.1]
* Update Prometheus to 2.50.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.50.1)
* [BUGFIX] API: Fix metadata API using wrong field names. #13633

[1.30.0]
* Update Prometheus to 2.51.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.51.0)

[1.30.1]
* Update Prometheus to 2.51.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.51.1)
* [BUGFIX] PromQL: Re-instate validation of `label_join` destination label #13803
* [BUGFIX] Scraping (experimental native histograms): Fix handling of the min bucket factor on sync of targets #13846
* [BUGFIX] PromQL: Some queries could return the same series twice (library use only) #13845

[1.30.2]
* Update Prometheus to 2.51.2
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.51.2)
* Notifier: could hang when using relabeling on alerts

[1.31.0]
* Update Prometheus to 2.52.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.52.0)
* [CHANGE] TSDB: Fix the predicate checking for blocks which are beyond the retention period to include the ones right at the retention boundary. #9633
* [FEATURE] Kubernetes SD: Add a new metric prometheus_sd_kubernetes_failures_total to track failed requests to Kubernetes API. #13554
* [FEATURE] Kubernetes SD: Add node and zone metadata labels when using the endpointslice role. #13935
* [FEATURE] Azure SD/Remote Write: Allow usage of Azure authorization SDK. #13099
* [FEATURE] Alerting: Support native histogram templating. #13731
* [FEATURE] Linode SD: Support IPv6 range discovery and region filtering. #13774
* [ENHANCEMENT] PromQL: Performance improvements for queries with regex matchers. #13461
* [ENHANCEMENT] PromQL: Performance improvements when using aggregation operators. #13744
* [ENHANCEMENT] PromQL: Validate label_join destination label. #13803
* [ENHANCEMENT] Scrape: Increment prometheus_target_scrapes_sample_duplicate_timestamp_total metric on duplicated series during one scrape. #12933
* [ENHANCEMENT] TSDB: Many improvements in performance. #13742 #13673 #13782
* [ENHANCEMENT] TSDB: Pause regular block compactions if the head needs to be compacted (prioritize head as it increases memory consumption). #13754
* [ENHANCEMENT] Observability: Improved logging during signal handling termination. #13772
* [ENHANCEMENT] Observability: All log lines for drop series use "num_dropped" key consistently. #13823
* [ENHANCEMENT] Observability: Log chunk snapshot and mmaped chunk replay duration during WAL replay. #13838
* [ENHANCEMENT] Observability: Log if the block is being created from WBL during compaction. #13846
* [BUGFIX] PromQL: Fix inaccurate sample number statistic when querying histograms. #13667
* [BUGFIX] PromQL: Fix histogram_stddev and histogram_stdvar for cases where the histogram has negative buckets. #13852
* [BUGFIX] PromQL: Fix possible duplicated label name and values in a metric result for specific queries. #13845
* [BUGFIX] Scrape: Fix setting native histogram schema factor during scrape. #13846
* [BUGFIX] TSDB: Fix counting of histogram samples when creating WAL checkpoint stats. #13776
* [BUGFIX] TSDB: Fix cases of compacting empty heads. #13755
* [BUGFIX] TSDB: Count float histograms in WAL checkpoint. #13844
* [BUGFIX] Remote Read: Fix memory leak due to broken requests. #13777
* [BUGFIX] API: Stop building response for /api/v1/series/ when the API request was cancelled. #13766
* [BUGFIX] promtool: Fix panic on promtool tsdb analyze --extended when no native histograms are present. #13976

[1.32.0]
* Update Prometheus to 2.53.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.53.0)
* [FEATURE] Rules: Add new option query_offset for each rule group via rule group configuration file and rule_query_offset as part of the global configuration to have more resilience for remote write delays. #14061 #14216 #14273
* [ENHANCEMENT] Rules: Add rule_group_last_restore_duration_seconds metric to measure the time it takes to restore a rule group. #13974
* [ENHANCEMENT] OTLP: Improve remote write format translation performance by using label set hashes for metric identifiers instead of string based ones. #14006 #13991
* [ENHANCEMENT] TSDB: Optimize querying with regexp matchers. #13620

[1.32.1]
* Update Prometheus to 2.53.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.53.1)

[1.33.0]
* Update Prometheus to 2.54.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.54.0)

[1.33.1]
* Update Prometheus to 2.54.1
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.54.1)

[1.34.0]
* Update Prometheus to 2.55.0
* [Full changelog](https://github.com/prometheus/prometheus/releases/tag/v2.55.0)

[1.34.1]
* Update Prometheus Server to 2.55.1
* [Full Changelog](https://github.com/prometheus/prometheus/releases/tag/v2.55.1)
* 

[2.0.0]
* Update prometheus to 3.0.0
* [Announcement](https://prometheus.io/blog/2024/11/14/prometheus-3-0/)
* [Migration Guide](https://prometheus.io/docs/prometheus/3.0/migration/)
* New, more inclusive governance
* More OpenTelemetry compatibility and features
* OpenMetrics 2.0, now under Prometheus governance!
* Native Histograms stability (and with custom buckets!)
* More optimizations!
* UTF-8 support coverage in more SDKs and tools

[2.0.1]
* Update prometheus to 3.0.1
* [Full Changelog](https://prometheus.io/blog/2024/11/14/prometheus-3-0/)
* \[BUGFIX] Promql: Make subqueries left open. [#&#8203;15431](https://github.com/prometheus/prometheus/issues/15431)
* \[BUGFIX] Fix memory leak when query log is enabled. [#&#8203;15434](https://github.com/prometheus/prometheus/issues/15434)
* \[BUGFIX] Support utf8 names on /v1/label/:name/values endpoint. [#&#8203;15399](https://github.com/prometheus/prometheus/issues/15399)

[2.1.0]
* Update prometheus to 3.1.0
* [Full Changelog](https://prometheus.io/blog/2024/11/14/prometheus-3-0/)
* \[SECURITY] upgrade golang.org/x/crypto to address reported CVE-2024-45337. [#&#8203;15691](https://github.com/prometheus/prometheus/issues/15691)
* \[CHANGE] Notifier: Increment prometheus_notifications_errors_total by the number of affected alerts rather than per batch. [#&#8203;15428](https://github.com/prometheus/prometheus/issues/15428)
* \[CHANGE] API: list rules field "groupNextToken:omitempty" renamed to "groupNextToken". [#&#8203;15400](https://github.com/prometheus/prometheus/issues/15400)
* \[ENHANCEMENT] OTLP translate: keep identifying attributes in target_info. [#&#8203;15448](https://github.com/prometheus/prometheus/issues/15448)
* \[ENHANCEMENT] Paginate rule groups, add infinite scroll to rules within groups. [#&#8203;15677](https://github.com/prometheus/prometheus/issues/15677)
* \[ENHANCEMENT] TSDB: Improve calculation of space used by labels. [#&#8203;13880](https://github.com/prometheus/prometheus/issues/13880)
* \[ENHANCEMENT] Rules: new metric rule_group_last_rule_duration_sum_seconds. [#&#8203;15672](https://github.com/prometheus/prometheus/issues/15672)
* \[ENHANCEMENT] Observability: Export 'go_sync_mutex_wait_total_seconds_total' metric. [#&#8203;15339](https://github.com/prometheus/prometheus/issues/15339)
* \[ENHANCEMEN] Remote-Write: optionally use a DNS resolver that picks a random IP. [#&#8203;15329](https://github.com/prometheus/prometheus/issues/15329)

[2.2.0]
* Update prometheus to 3.2.0
* [Full Changelog](https://github.com/prometheus/prometheus/issues/15400)
* \[CHANGE] relabel: Replace actions can now use UTF-8 characters in `targetLabel` field. Note that `$<chars>` or `${<chars>}` will be expanded. This also apply to `replacement` field for `LabelMap` action. [#&#8203;15851](https://github.com/prometheus/prometheus/issues/15851)
* \[CHANGE] rulefmt: Rule names can use UTF-8 characters, except `{` and `}` characters (due to common mistake checks). [#&#8203;15851](https://github.com/prometheus/prometheus/issues/15851)
* \[FEATURE] remote/otlp: Add feature flag `otlp-deltatocumulative` to support conversion from delta to cumulative. [#&#8203;15165](https://github.com/prometheus/prometheus/issues/15165)
* \[ENHANCEMENT] openstack SD: Discover Octavia loadbalancers. [#&#8203;15539](https://github.com/prometheus/prometheus/issues/15539)
* \[ENHANCEMENT] scrape: Add metadata for automatic metrics to WAL for `metadata-wal-records` feature. [#&#8203;15837](https://github.com/prometheus/prometheus/issues/15837)
* \[ENHANCEMENT] promtool: Support linting of scrape interval, through lint option `too-long-scrape-interval`. [#&#8203;15719](https://github.com/prometheus/prometheus/issues/15719)
* \[ENHANCEMENT] promtool: Add --ignore-unknown-fields option. [#&#8203;15706](https://github.com/prometheus/prometheus/issues/15706)
* \[ENHANCEMENT] ui: Make "hide empty rules" and hide empty rules" persistent [#&#8203;15807](https://github.com/prometheus/prometheus/issues/15807)
* \[ENHANCEMENT] web/api: Add a limit parameter to `/query` and `/query_range`. [#&#8203;15552](https://github.com/prometheus/prometheus/issues/15552)
* \[ENHANCEMENT] api: Add fields Node and ServerTime to `/status`. [#&#8203;15784](https://github.com/prometheus/prometheus/issues/15784)
* \[PERF] Scraping: defer computing labels for dropped targets until they are needed by the UI.  [#&#8203;15261](https://github.com/prometheus/prometheus/issues/15261)
* \[BUGFIX] remotewrite2: Fix invalid metadata bug for metrics without metadata. [#&#8203;15829](https://github.com/prometheus/prometheus/issues/15829)
* \[BUGFIX] remotewrite2: Fix the unit field propagation. [#&#8203;15825](https://github.com/prometheus/prometheus/issues/15825)
* \[BUGFIX] scrape: Fix WAL metadata for histograms and summaries. [#&#8203;15832](https://github.com/prometheus/prometheus/issues/15832)
* \[BUGFIX] ui: Merge duplicate "Alerts page settings" sections. [#&#8203;15810](https://github.com/prometheus/prometheus/issues/15810)
* \[BUGFIX] PromQL: Fix `<aggr_over_time>` functions with histograms. [#&#8203;15711](https://github.com/prometheus/prometheus/issues/15711)

[2.2.1]
* Update prometheus to 3.2.1
* [Full Changelog](https://github.com/prometheus/prometheus/issues/15851)
* \[BUGFIX] Don't send Accept`header`escape=allow-utf-8`when`metric_name_validation_scheme: legacy\` is configured. [#&#8203;16061](https://github.com/prometheus/prometheus/issues/16061)

[2.3.0]
* Update base image to 5.0.0

